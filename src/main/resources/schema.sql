DROP TABLE IF EXISTS Employee;

CREATE TABLE Employee( 
    id varchar(4), 
    name varchar(10),
    dob varchar(10),
    salary int);

DROP TABLE IF EXISTS Customer;

CREATE TABLE Customer(
  id varchar (4),
  name varchar(10)
)